#!/bin/bash

# note: we use cross to build against a consistent environment
# todo: use nix instead
#       (nix is better for this but i am allergic to knowledge)

cross build --package client --target x86_64-unknown-linux-gnu --release
cross build --package server --target x86_64-unknown-linux-gnu --release
cross build --package client --target x86_64-unknown-linux-musl --release
cross build --package server --target x86_64-unknown-linux-musl --release

mkdir -p artifacts
cp target/x86_64-unknown-linux-gnu/release/client artifacts/x86_64-unknown-linux-gnu-client
cp target/x86_64-unknown-linux-gnu/release/server artifacts/x86_64-unknown-linux-gnu-server
cp target/x86_64-unknown-linux-musl/release/client artifacts/x86_64-unknown-linux-musl-client
cp target/x86_64-unknown-linux-musl/release/server artifacts/x86_64-unknown-linux-musl-server

rm -r target/x86_64-unknown-linux-gnu
rm -r target/x86_64-unknown-linux-musl

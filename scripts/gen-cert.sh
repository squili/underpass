#!/bin/sh

openssl ecparam -out ca/$1.key -name secp384r1 -genkey
openssl req -new -key ca/$1.key -addext "subjectAltName = DNS:$2" -out ca/$1.csr -subj "/C=E2/ST=Denial/O=Mia Fanclub"
openssl x509 -req -in ca/$1.csr -CA ca/ca.crt -CAkey ca/ca.key -CAcreateserial -extfile <(printf "subjectAltName=DNS:$2") -out ca/$1.crt

#!/bin/sh

mkdir -p ca
openssl ecparam -out ca/ca.key -name secp384r1 -genkey
openssl req -new -noenc -x509 -subj "/C=E2/ST=Denial/O=Mia Fanclub" -key ca/ca.key -out ca/ca.crt

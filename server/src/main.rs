// underpass reverse forwards reverse proxy proxy
// Copyright (C) 2023 Mia
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{net::SocketAddr, path::PathBuf, str::FromStr, sync::Arc};

use eyre::{Context, ContextCompat};
use parking_lot::Mutex;
use tokio::{
    io::AsyncWriteExt,
    net::{TcpListener, TcpStream},
    select,
    sync::Notify,
};
use tokio_rustls::{
    rustls::{
        server::AllowAnyAuthenticatedClient, Certificate, PrivateKey, RootCertStore, ServerConfig,
    },
    server::TlsStream,
    TlsAcceptor,
};
use tracing::{error, info};

#[derive(knuffel::Decode)]
struct Config {
    #[knuffel(child, unwrap(argument))]
    proxy_server: String,
    #[knuffel(child, unwrap(argument))]
    ca_cert: PathBuf,
    #[knuffel(child, unwrap(argument))]
    server_cert: PathBuf,
    #[knuffel(child, unwrap(argument))]
    server_key: PathBuf,
    #[knuffel(children(name = "forward"))]
    forwards: Vec<Forward>,
}

#[derive(knuffel::Decode)]
struct Forward {
    #[knuffel(property)]
    from: String,
    #[knuffel(property)]
    to: u16,
}

#[tokio::main]
async fn main() -> eyre::Result<()> {
    println!("underpass v{}", env!("CARGO_PKG_VERSION"));

    color_eyre::install()?;
    tracing_subscriber::fmt::init();

    let Some(config_path) = std::env::args().nth(1) else {
        eyre::bail!("please specify a config path");
    };
    let config_path = PathBuf::from_str(&config_path)?;

    let config: Config = knuffel::parse(
        &config_path.file_name().unwrap().to_string_lossy(),
        &std::fs::read_to_string(&config_path)?,
    )
    .map_err(|err| eyre::eyre!(format!("{:?}", miette::Report::new(err))))?;

    let proxy_server = config
        .proxy_server
        .parse()
        .wrap_err("while parsing proxy_server")?;

    let ca_certs = rustls_pemfile::certs(&mut std::fs::read(&config.ca_cert)?.as_ref())?;
    let server_certs = rustls_pemfile::certs(&mut std::fs::read(&config.server_cert)?.as_ref())?;
    let server_key =
        rustls_pemfile::ec_private_keys(&mut std::fs::read(&config.server_key)?.as_ref())?
            .into_iter()
            .next()
            .wrap_err("expected private key file to contain at least one item")?;

    let mut root_cert_store = RootCertStore::empty();
    for cert in ca_certs {
        root_cert_store.add(&Certificate(cert))?;
    }

    let server_config = ServerConfig::builder()
        .with_safe_defaults()
        .with_client_cert_verifier(Arc::new(AllowAnyAuthenticatedClient::new(root_cert_store)))
        .with_single_cert(
            server_certs.into_iter().map(Certificate).collect(),
            PrivateKey(server_key),
        )?;
    let acceptor = TlsAcceptor::from(Arc::new(server_config));

    let broker = Arc::new(ConnectionBroker::default());

    let provider_task = tokio::spawn(provide_connections(broker.clone(), proxy_server, acceptor));
    let mut consumer_tasks = Vec::new();
    for forward in config.forwards {
        let listener = TcpListener::bind(forward.from).await?;
        consumer_tasks.push(tokio::spawn(consume_connections(
            broker.clone(),
            listener,
            forward.to,
        )));
    }
    let consumer_task = futures_util::future::select_all(consumer_tasks);

    select! {
        res = provider_task => res?.wrap_err("in provider task"),
        res = consumer_task => res.0?.wrap_err("in a consumer task"),
    }
}

#[derive(Default)]
struct ConnectionBroker {
    connections: Mutex<Vec<TlsStream<TcpStream>>>,
    notify: Notify,
}

impl ConnectionBroker {
    pub fn provide(&self, conn: TlsStream<TcpStream>) {
        info!("new connection provided");

        self.connections.lock().push(conn);
        self.notify.notify_one();
    }

    #[inline(always)]
    fn try_get(&self) -> Option<TlsStream<TcpStream>> {
        self.connections.lock().pop()
    }

    pub async fn get(&self) -> TlsStream<TcpStream> {
        let future = self.notify.notified();
        tokio::pin!(future);

        loop {
            future.as_mut().enable();

            if let Some(conn) = self.try_get() {
                return conn;
            }

            future.as_mut().await;

            future.set(self.notify.notified());
        }
    }
}

async fn provide_connections(
    broker: Arc<ConnectionBroker>,
    proxy_server: SocketAddr,
    acceptor: TlsAcceptor,
) -> eyre::Result<()> {
    let listener = TcpListener::bind(proxy_server).await?;

    loop {
        let (stream, addr) = listener.accept().await?;

        info!(%addr, "new provider connection");

        match acceptor.accept(stream).await {
            Ok(stream) => broker.provide(stream),
            Err(err) => error!(%err, "error accepting connection on {addr}"),
        }
    }
}

async fn consume_connections(
    broker: Arc<ConnectionBroker>,
    listener: TcpListener,
    proxy_to: u16,
) -> eyre::Result<()> {
    loop {
        let (local_stream, addr) = listener.accept().await?;

        info!(%addr, "new consumer connection");

        let mut proxy_connection = broker.get().await;

        proxy_connection.write_u16(proxy_to).await?;
        proxy_connection.flush().await?;

        tokio::spawn(async move {
            let (mut local_reader, mut local_writer) = local_stream.into_split();
            let (mut proxy_reader, mut proxy_writer) = tokio::io::split(proxy_connection);

            let copy_local_to_proxy = tokio::io::copy(&mut local_reader, &mut proxy_writer);
            let copy_proxy_to_local = tokio::io::copy(&mut proxy_reader, &mut local_writer);

            if let Err((direction, err)) = select! {
                res = copy_local_to_proxy => {
                    let _ = local_writer.shutdown().await;
                    let _ = proxy_writer.shutdown().await;
                    res.map_err(|err| ("local to proxy", err))
                }
                res = copy_proxy_to_local => {
                    let _ = local_writer.shutdown().await;
                    let _ = proxy_writer.shutdown().await;
                    res.map_err(|err| ("proxy to local", err))
                }
            } {
                match err.kind() {
                    std::io::ErrorKind::ConnectionReset => {}
                    _ => {
                        error!(?err, ?direction, "error copying data");
                    }
                }
            }
        });
    }
}

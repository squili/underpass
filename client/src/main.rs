// underpass reverse forwards reverse proxy proxy
// Copyright (C) 2023 Mia
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{
    net::{IpAddr, Ipv4Addr, SocketAddr, ToSocketAddrs},
    path::PathBuf,
    str::FromStr,
    sync::Arc,
    time::Duration,
};

use eyre::{Context, ContextCompat};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
    select,
};
use tokio_rustls::{
    client::TlsStream,
    rustls::{Certificate, ClientConfig, PrivateKey, RootCertStore, ServerName},
    TlsConnector,
};
use tracing::{debug, error, info};

#[derive(knuffel::Decode)]
struct Config {
    #[knuffel(child, unwrap(argument))]
    proxy_server: String,
    #[knuffel(child, unwrap(argument))]
    ca_cert: PathBuf,
    #[knuffel(child, unwrap(argument))]
    client_cert: PathBuf,
    #[knuffel(child, unwrap(argument))]
    client_key: PathBuf,
    #[knuffel(child, unwrap(argument))]
    server_name: String,
    #[knuffel(child, unwrap(arguments))]
    allowed_ports: Vec<u16>,
    #[knuffel(child, unwrap(argument))]
    pid_file: Option<PathBuf>,
}

#[tokio::main]
async fn main() -> eyre::Result<()> {
    println!("underpass v{}", env!("CARGO_PKG_VERSION"));

    color_eyre::install()?;
    tracing_subscriber::fmt::init();

    let Some(config_path) = std::env::args().nth(1) else {
        eyre::bail!("please specify a config path");
    };
    let config_path = PathBuf::from_str(&config_path)?;

    let config: Config = knuffel::parse(
        &config_path.file_name().unwrap().to_string_lossy(),
        &std::fs::read_to_string(&config_path)?,
    )
    .map_err(|err| eyre::eyre!(format!("{:?}", miette::Report::new(err))))?;

    if let Some(pid_file) = config.pid_file {
        std::fs::write(pid_file, std::process::id().to_string())?;
    }

    let ca_certs = rustls_pemfile::certs(&mut std::fs::read(&config.ca_cert)?.as_ref())?;
    let client_certs = rustls_pemfile::certs(&mut std::fs::read(&config.client_cert)?.as_ref())?;
    let client_key =
        rustls_pemfile::ec_private_keys(&mut std::fs::read(&config.client_key)?.as_ref())?
            .into_iter()
            .next()
            .wrap_err("expected private key file to contain at least one item")?;

    let mut root_cert_store = RootCertStore::empty();
    for cert in ca_certs {
        root_cert_store.add(&Certificate(cert))?;
    }

    let client_config = ClientConfig::builder()
        .with_safe_defaults()
        .with_root_certificates(root_cert_store)
        .with_single_cert(
            client_certs.into_iter().map(Certificate).collect(),
            PrivateKey(client_key),
        )?;

    let connector = TlsConnector::from(Arc::new(client_config));
    let server_name = ServerName::try_from(config.server_name.as_str())?;
    let mut error_count = None;

    loop {
        match connect_to_proxy(
            &config.proxy_server,
            &connector,
            &server_name,
            &config.allowed_ports,
        )
        .await
        {
            Ok(_) => error_count = None,
            Err(err) => {
                error!(?err, "error trying to connect to proxy");
                let retries = match &error_count {
                    Some(n) if *n < 6 => n + 1,
                    Some(_) => 6,
                    None => 1,
                };
                error_count = Some(retries);
                let sleep_for = Duration::from_secs(2_u64.pow(retries));
                info!(?sleep_for, "sleeping");
                tokio::time::sleep(sleep_for).await;
            }
        }
    }
}

async fn connect_to_proxy(
    proxy_server: &str,
    connector: &TlsConnector,
    server_name: &ServerName,
    allowed_ports: &[u16],
) -> eyre::Result<()> {
    info!("starting new connection");
    let stream = TcpStream::connect(
        proxy_server
            .to_socket_addrs()?
            .into_iter()
            .filter(|addr| addr.is_ipv4())
            .next()
            .wrap_err("server has no ipv4 addresses")?,
    )
    .await?;
    info!("performing tls handshake");
    let mut stream = connector
        .connect(server_name.clone(), stream)
        .await
        .wrap_err("during tls handshake")?;
    info!("waiting for target port");
    let target_port = stream.read_u16().await.wrap_err("reading target port")?;
    debug!(?target_port, "received target port");
    if !allowed_ports.contains(&target_port) {
        eyre::bail!("port {} not allowed", target_port);
    }

    tokio::spawn(handle_proxy_connection(stream, target_port));

    Ok(())
}

async fn handle_proxy_connection(proxy_stream: TlsStream<TcpStream>, target_port: u16) {
    let socket_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::LOCALHOST), target_port);
    debug!(?socket_addr, "connection");
    let local_stream = match TcpStream::connect(socket_addr).await {
        Ok(s) => s,
        Err(err) => {
            error!(?err, "error connecting to target");
            return;
        }
    };

    let (mut local_reader, mut local_writer) = local_stream.into_split();
    let (mut proxy_reader, mut proxy_writer) = tokio::io::split(proxy_stream);

    let copy_local_to_proxy = tokio::io::copy(&mut local_reader, &mut proxy_writer);
    let copy_proxy_to_local = tokio::io::copy(&mut proxy_reader, &mut local_writer);

    if let Err((direction, err)) = select! {
        res = copy_local_to_proxy => {
            let _ = local_writer.shutdown().await;
            let _ = proxy_writer.shutdown().await;
            res.map_err(|err| ("local to proxy", err))
        }
        res = copy_proxy_to_local => {
            let _ = local_writer.shutdown().await;
            let _ = proxy_writer.shutdown().await;
            res.map_err(|err| ("proxy to local", err))
        }
    } {
        match err.kind() {
            std::io::ErrorKind::ConnectionReset => {}
            _ => {
                error!(?err, ?direction, "error copying data");
            }
        }
    }
}
